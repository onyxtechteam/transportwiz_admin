// The Cloud Functions for Firebase SDK to create Cloud Functions and set up triggers.
const functions = require('firebase-functions');
const crypto = require("crypto");
const admin = require('firebase-admin');

//listen to new taxi order
exports.newDriverData = functions.firestore.document('/drivers/{documentId}')
    .onCreate(async (snap, context) => {

        // taxi order data
        let driverId = context.params.documentId;
        const prevTaxiOrder = snap.before.data();
        const newDriverData = snap.after.data();
        //if driver is frr and online 
        if (newDriverData.free == 1 && newDriverData.online == 1) {
            //get the nearby new taxi order
            let driverSearchRrange = newDriverData.range ? newDriverData.range : 10;
            let earthDistanceNorth = newDriverData.earth_distance + driverSearchRrange;
            let earthDistanceSouth = newDriverData.earth_distance - driverSearchRrange;
            //
            const db = admin.firestore();
            const newTaxiOrdersRef = db.collection('newTaxiOrders');
            const newTaxiOrder = await newTaxiOrdersRef.where('earth_distance', '<=', earthDistanceNorth)
                .where('earth_distance', '>=', earthDistanceSouth)
                //if driver is online
                .where('online', "==", 1)
                //if driver has not trip
                .where('free', "==", 1)
                //same vehicle type 
                .where("vehicle_type_id", "==", newDriverData.vehicle_type_id)
                .limit(1)
                .get();

            //
            if (newTaxiOrder.empty) {
                console.log('No new taxi order within range');
                return;
            }

            //loop through drivers, then call api to send notification to drivers
            //if driver is in the ingored drivers, then skip the loop
            if (newTaxiOrder.ignoredDrivers.includes(driverId)) {
                return;
            }
            //notification data
            let notificationData = {
                pickup: JSON.stringify(newTaxiOrder.pickup),
                dropoff: JSON.stringify(newTaxiOrder.dropoff),
                amount: newTaxiOrder.amount.toString(),
                total: newTaxiOrder.total.toString(),
                id: newTaxiOrder.id.toString(),
                range: newTaxiOrder.range.toString(),
                status: newTaxiOrder.status.toString(),
                trip_distance: newTaxiOrder.trip_distance.toString(),
                code: newTaxiOrder.trip_distance.toString(),
                vehicle_type_id: newTaxiOrder.vehicle_type_id.toString(),
                earth_distance: newTaxiOrder.earth_distance.toString()
            };
            //notification message
            const message = {
                data: notificationData,
                topic: "d_" + driverId + "",
            };

            //
            console.log('Notifying driver ==> ', driverId);
            const writeResult = await admin.messaging().send(message).then((response) => {
                // Response is a message ID string.
                console.log('Successfully sent message:', response);
            }).catch((error) => {
                console.log('Error sending message:', error);
            });

        }
        return true;
    });


//order ignoredDrivers updated
exports.driverDataUpdated = functions.firestore.document('/drivers/{documentId}')
    .onUpdate(async (snap, context) => {

        // taxi order data
        let driverId = context.params.documentId;
        const prevTaxiOrder = snap.before.data();
        const newDriverData = snap.after.data();
        //if driver is frr and online 
        if (newDriverData.free == 1 && newDriverData.online == 1) {
            //get the nearby new taxi order
            let driverSearchRrange = newDriverData.range ? newDriverData.range : 10;
            let earthDistanceNorth = newDriverData.earth_distance + driverSearchRrange;
            let earthDistanceSouth = newDriverData.earth_distance - driverSearchRrange;
            console.log('driverSearchRrange', driverSearchRrange,
                earthDistanceNorth,
                earthDistanceSouth);
            //
            const db = admin.firestore();
            const newTaxiOrdersRef = db.collection('newTaxiOrders');
            const newTaxiOrder = await newTaxiOrdersRef.where('earth_distance', '<=', earthDistanceNorth)
                .where('earth_distance', '>=', earthDistanceSouth)
                //same vehicle type 
                .where("vehicle_type_id", "==", newDriverData.vehicle_type_id)
                .limit(1)
                .get();

            //
            if (newTaxiOrder.empty) {
                console.log('No new taxi order within range');
                return;
            }

            //loop through drivers, then call api to send notification to drivers
            //if driver is in the ingored drivers, then skip the loop
            if (newTaxiOrder.ignoredDrivers == null || newTaxiOrder.ignoredDrivers.includes(driverId)) {
                return;
            }
            //notification data
            let notificationData = {
                pickup: JSON.stringify(newTaxiOrder.pickup),
                dropoff: JSON.stringify(newTaxiOrder.dropoff),
                amount: newTaxiOrder.amount.toString(),
                total: newTaxiOrder.total.toString(),
                id: newTaxiOrder.id.toString(),
                range: newTaxiOrder.range.toString(),
                status: newTaxiOrder.status.toString(),
                trip_distance: newTaxiOrder.trip_distance.toString(),
                code: newTaxiOrder.trip_distance.toString(),
                vehicle_type_id: newTaxiOrder.vehicle_type_id.toString(),
                earth_distance: newTaxiOrder.earth_distance.toString()
            };
            //notification message
            const message = {
                data: notificationData,
                topic: "d_" + driverId + "",
            };

            //
            console.log('Notifying driver ==> ', driverId);
            const writeResult = await admin.messaging().send(message).then((response) => {
                // Response is a message ID string.
                console.log('Successfully sent message:', response);
            }).catch((error) => {
                console.log('Error sending message:', error);
            });

        }
        return true;
    });




