<?php

namespace App\Models;


class Service extends NoDeleteBaseModel
{
    protected $fillable = [
        "name", "description", "vendor_id", "category_id", "price", "discount_price", "per_hour", "is_active",
    ];
    protected $appends = ['formatted_date', 'photo', 'photos'];
    protected $with = ['vendor', 'category'];

    public function vendor()
    {
        return $this->belongsTo('App\Models\Vendor', 'vendor_id', 'id');
    }


    public function category()
    {
        return $this->belongsTo('App\Models\Category', 'category_id', 'id');
    }

    public function sales()
    {
        return $this->hasMany('App\Models\OrderService', 'service_id', 'id');
    }

    public function getPhotosAttribute(){
        $mediaItems = $this->getMedia('default');
        $photos = [];

        foreach ($mediaItems as $mediaItem) {
            array_push($photos, $mediaItem->getFullUrl());
        }
        return $photos;
    }

}
